resource "aws_iam_role_policy_attachment" "read_only" {
  policy_arn = data.aws_iam_policy.read_only.arn
  role       = var.iam_role_id
}
