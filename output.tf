output "part" {
  description = "The rendered cloud init script to be run on the instance(s) when it launches."
  value = {
    filename     = "configure-https.sh"
    content_type = "text/x-shellscript"
    content = templatefile("${path.module}/configure-https.sh", {
      var_region       = var.region
      var_bucket_id    = data.aws_s3_bucket.primary.id
      var_cert_folder  = var.cert_folder
      var_cert_subject = var.cert_subject
      var_start_nginx  = var.start_nginx ? "true" : "false"
    })
  }
}
