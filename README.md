# cloud-init-acme-nginx

## Summary

Exports a cloud-init script that provisions Let's Encrypt certificates in an EC2 instance specifically for the nginx web server.

Prior to using this module, the `acme-lambda-config` module must be configured with the certificate details. The `acme-lambda` scans the S3 bucket daily for YML files and generates certificates for the configured domains. After the lambda executes, the S3 bucket will contain the public and private key pairs for the provisioned certificates.

The EC2 instance will have a cron job that downloads the updated certificates daily and reloads the nginx configuration. Also nginx is configured to redirect all HTTP trafic to HTTPS.

More information about the free Let's Encrypt certificates is available [here](https://letsencrypt.org/).

## Resources

This module doesn't create any resources.

## Cost

Using this module on its own doesn't cost anything.

## Inputs

| Name         | Description                                                                                                                       | Type     | Default                  | Required |
|:-------------|:----------------------------------------------------------------------------------------------------------------------------------|:---------|:-------------------------|:---------|
| region       | AWS region where to create resources.                                                                                             | `string` | `us-east-1`              | yes      |
| lambda_name  | Specifies the name of the lambda that automates Let's Encrypt certificates.                                                       | `string` | `acme-management-lambda` | yes      |
| iam_role_id  | ID of the IAM role for the EC2 instance(s) which policies are attached to.                                                        | `string` | -                        | yes      |
| cert_folder  | Specifies the folder in S3 where to download the certificates from. A common convention is to use the certificate's subject name. | `string` | -                        | yes      |
| cert_subject | Specifies the subject name for the provisioned certificate.                                                                       | `string` | -                        | yes      |
| start_nginx  | Specifies whether the nginx service should be started on the EC2 instance.                                                        | `bool`   | -                        | yes      |

## Outputs

| Name | Description                                                                   |
|:-----|:------------------------------------------------------------------------------|
| part | The rendered cloud init script to be run on the instance(s) when it launches. |

## Examples

### Simple Usage

```terraform
module "cloud_init_acme_nginx" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/cloud-init-acme-nginx.git?ref=v1"

  iam_role_id  = "todo"
  cert_folder  = "example"
  cert_subject = "example.cloud"
  start_nginx  = true
}

data "template_cloudinit_config" "example" {
  part {
    filename     = module.cloud_init_dns.part.filename
    content_type = module.cloud_init_dns.part.content_type
    content      = module.cloud_init_dns.part.content
  }
}

resource "aws_instance" "example" {
  user_data              = data.template_cloudinit_config.example.rendered

  # other properties omitted for brevity
  # ...
}
```

## Version History

* v1 - Initial Release
