variable "region" {
  type        = string
  default     = "us-east-1"
  description = "(Optional; Default: us-east-1) AWS region where to create resources."
}

variable "lambda_name" {
  type        = string
  default     = "acme-management-lambda"
  description = "(Optional; Default: acme-management-lambda)Specifies the name of the lambda that automates Let's Encrypt certificates."
}

variable "iam_role_id" {
  type        = string
  description = "ID of the IAM role for the EC2 instance(s) which policies are attached to."
}

variable "cert_folder" {
  type        = string
  description = "Specifies the folder in S3 where to download the certificates from. A common convention is to use the certificate's subject name."
}

variable "cert_subject" {
  type        = string
  description = "Specifies the subject name for the provisioned certificate."
}

variable "start_nginx" {
  type        = bool
  description = "Specifies whether the nginx service should be started on the EC2 instance."
}
