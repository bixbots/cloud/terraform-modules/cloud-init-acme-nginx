data "aws_caller_identity" "current" {}

data "aws_lambda_function" "primary" {
  function_name = var.lambda_name
}

data "aws_s3_bucket" "primary" {
  bucket = data.aws_lambda_function.primary.environment[0].variables["BUCKET_ID"]
}

data "aws_iam_policy" "read_only" {
  arn = data.aws_lambda_function.primary.environment[0].variables["READ_ONLY_POLICY_ARN"]
}
