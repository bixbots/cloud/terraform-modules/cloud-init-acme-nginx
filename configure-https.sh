#!/usr/bin/env bash

VAR_REGION='${var_region}'
VAR_BUCKET_ID='${var_bucket_id}'
VAR_CERT_FOLDER='${var_cert_folder}'
VAR_CERT_SUBJECT='${var_cert_subject}'
VAR_START_NGINX='${var_start_nginx}'

export AWS_DEFAULT_REGION="$VAR_REGION"

sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm > /dev/null
sudo yum-config-manager --enable epel > /dev/null
sudo yum update -y > /dev/null

sudo yum install -y amazon-linux-extras > /dev/null
sudo yum install -y nginx > /dev/null
sudo amazon-linux-extras install -y php7.4 > /dev/null

sudo mkdir -p /etc/pki/nginx/{certs,private} > /dev/null

sudo aws s3 cp s3://$VAR_BUCKET_ID/$VAR_CERT_FOLDER/$VAR_CERT_SUBJECT.chain.pem /etc/pki/nginx/certs/
sudo aws s3 cp s3://$VAR_BUCKET_ID/$VAR_CERT_FOLDER/$VAR_CERT_SUBJECT.key.pem /etc/pki/nginx/private/

cat > /etc/pki/nginx/s3-sync.sh <<EOF
#!/usr/bin/env bash
aws s3 cp s3://$VAR_BUCKET_ID/$VAR_CERT_FOLDER/$VAR_CERT_SUBJECT.chain.pem /etc/pki/nginx/certs/
aws s3 cp s3://$VAR_BUCKET_ID/$VAR_CERT_FOLDER/$VAR_CERT_SUBJECT.key.pem /etc/pki/nginx/private/
nginx -t && systemctl reload nginx
EOF
sudo chmod +x /etc/pki/nginx/s3-sync.sh

cat > /etc/cron.d/pki-nginx-s3-sync <<EOF
30 3 * * * root /etc/pki/nginx/s3-sync.sh
EOF

sudo chmod 700 /etc/pki/nginx/private
sudo chmod 600 /etc/pki/nginx/private/*
sudo chown nginx.nginx -R /etc/pki/nginx/

# Credits to:
# https://gist.github.com/cecilemuller/a26737699a7e70a7093d4dc115915de8
# https://haydenjames.io/nginx-tuning-tips-tls-ssl-https-ttfb-latency/

cat > /etc/nginx/conf.d/ssl.conf <<EOF
server {
	listen       443 ssl http2 default_server;
	listen       [::]:443 ssl http2 default_server;
	server_name  _;

	root         /usr/share/nginx/html;

	ssl_certificate /etc/pki/nginx/certs/$VAR_CERT_SUBJECT.chain.pem;
	ssl_certificate_key /etc/pki/nginx/private/$VAR_CERT_SUBJECT.key.pem;
	ssl_trusted_certificate /etc/pki/nginx/certs/$VAR_CERT_SUBJECT.chain.pem;

	ssl_session_cache shared:le_nginx_SSL:1m;
	ssl_session_timeout 1d;
	ssl_session_tickets off;

	ssl_protocols TLSv1.2 TLSv1.3;
	ssl_prefer_server_ciphers on;
	ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
	ssl_ecdh_curve secp384r1;

	ssl_stapling on;
	ssl_stapling_verify on;

	add_header Strict-Transport-Security "max-age=31536000; includeSubdomains; preload;";
	add_header Content-Security-Policy "default-src 'self' https:; frame-ancestors 'none'; script-src 'self' https: 'unsafe-inline'; style-src 'self' https: 'unsafe-inline';";
	add_header Referrer-Policy "no-referrer, strict-origin-when-cross-origin";
	add_header X-Frame-Options SAMEORIGIN;
	add_header X-Content-Type-Options nosniff;
	add_header X-XSS-Protection "1; mode=block";

	# Load configuration files for the default server block.
	include /etc/nginx/default.d/*.conf;

	location / {
	}

	error_page 404 /404.html;
	location = /40x.html {
		internal;
	}

	error_page 500 502 503 504 /50x.html;
	location = /50x.html {
		internal;
	}
}
EOF

cat > /etc/nginx/default.d/http_to_https_redirect.conf <<EOF
if (\$scheme = http) {
	return 308 https://\$host\$request_uri;
}
EOF

sudo systemctl enable nginx

if [ "$VAR_START_NGINX" = "true" ]; then
	sudo systemctl start nginx
fi
